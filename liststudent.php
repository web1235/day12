<?php
$spec = array("MAT" => "Khoa học máy tính", "KDL" => "Khoa học vật liệu");
?>

<!DOCTYPE html>
<html lang="en">

<?php
    session_start();
    $faculty_arr = '';
    $key_word = '';

    if(isset($_SESSION["faculty_arr"])&&isset($_SESSION["key_word"])){
        $faculty_arr = $_SESSION["faculty_arr"];
        $key_word = $_SESSION["key_word"];
    }

    if(isset($_POST['search'])){
        if(isset($_POST['faculty_arr'])&&isset($_POST['key_word'])) {
            $faculty_arr = $_POST['faculty_arr'];
            $key_word = $_POST['key_word'];
            $_SESSION["faculty_arr"] = $_POST['faculty_arr'];
            $_SESSION["key_word"] = $_POST['key_word'];
        }
    } elseif (isset($_POST['delete'])){
        $faculty_arr = '';
        $key_word = '';
        $_SESSION["faculty_arr"] = '';
        $_SESSION["key_word"] = '';
    }

?>
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>List student</title>
    <link rel="stylesheet" href="styles.css"> 

    
  
</head>

<body>
    <div class="search-form">
        <form name="registerForm" method="POST" enctype="multipart/form-data" action="">
            <table>
                <tr class="form-item">
                    <td>
                        <p>
                            <label for="spec">
                                Khoa
                            </label>
                        </p>
                    </td>
                    <td>
                        <select name="faculty_arr" id="spec">
                            <?php
                                if($faculty_arr==''){
                                    $industryCodeArr=array("EMPTY"=>"","MAT"=>"Khoa học máy tính","KDL"=>"Khoa học vật liệu");
                                    foreach($industryCodeArr as $x=>$x_value){
                                        echo"<option>".$x_value."</option>";
                                    }
                                }else{
                                    echo "<option>".$faculty_arr."</option>";
                                }
                            ?>
                        </select>
                    </td>
                </tr>
                <tr>
                    <td class="form-item">
                        <p>
                            <label for="spec">
                                Từ khóa
                            </label>
                        </p>
                    </td>
                    <td>
                        <input type="text" name="key_word" value="<?php echo $key_word; ?>">
                    </td>
                </tr>
                <tr>
                    <td>
                    </td>
                    <td>
                        <input type="submit" name="delete" value="Xóa">
                        <input type="submit" name="search" value="Tìm kiếm">

                    </td>
                </tr>
            </table>
        </form>
    </div>
    <div class="result-search">
        <span>Số sinh viên tìm thấy:</span>
        <span>XXX</span>
    </div>
    <div class="list-student">
        <div class="wrap-action">
            <a class="action" href="./register.php">Thêm</a>
        </div>
        <div class="table-student">
            <table>
                <colgroup>
                    <col span="1" style="width: 10%;">
                    <col span="1" style="width: 25%;">
                    <col span="1" style="width: 50%;">
                    <col span="1" style="width: 15%;">
                </colgroup>
                <tr>
                    <th>No</th>
                    <th>Tên sinh viên</th>
                    <th>Khoa</th>
                    <th>Action</th>
                </tr>
                <tr>
                    <td>1</td>
                    <td>Nguyễn Văn A</td>
                    <td>Khoa học máy tính</td>
                    <td>
                        <a class="action" href="">Xóa</a>
                        <a class="action" href="">Sửa</a>
                    </td>
                </tr>
                <tr>
                    <td>2</td>
                    <td>Trần Thị C</td>
                    <td>Khoa học máy tính</td>
                    <td>
                        <a class="action" href="">Xóa</a>
                        <a class="action" href="">Sửa</a>
                    </td>
                </tr>
                <tr>
                    <td>3</td>
                    <td>Nguyễn Văn C</td>
                    <td>Khoa học vật liệu</td>
                    <td>
                        <a class="action" href="">Xóa</a>
                        <a class="action" href="">Sửa</a>
                    </td>
                </tr>
                <tr>
                    <td>4</td>
                    <td>Hoang Ngoc Anh</td>
                    <td>Khoa học vật liệu</td>
                    <td>
                        <a class="action" href="">Xóa</a>
                        <a class="action" href="">Sửa</a>
                    </td>
                </tr>
            </table>
        </div>
    </div>
</body>

</html>